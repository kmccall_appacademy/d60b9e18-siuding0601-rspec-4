class Temperature

  attr_accessor :f, :c

  def initialize(options = {})
    @f = options[:f]
    @c = options[:c]
  end

  def in_fahrenheit
    if @f != nil
      @f
    else
      (@c * 9/5.to_f) + 32
    end
  end

  def in_celsius
    if @c != nil
      @c
    else
      (@f - 32) * 5/9.to_f
    end
  end

  def self.from_celsius(degree)
    Temperature.new({c:degree})
  end

  def self.from_fahrenheit(degree)
    Temperature.new({f:degree})
  end
end

class Celsius < Temperature

  attr_accessor :c

  def initialize(degree)
    @c = degree
  end
end

class Fahrenheit < Temperature

  attr_accessor :f

  def initialize(degree)
    @f = degree
  end
end
