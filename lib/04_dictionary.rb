class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(option = {})
    if option.class == Hash
      @entries[option.keys[0]] = option.values[0]
    elsif option.class == String
      @entries[option] = nil
    end

  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    @entries.keys.include?(word)
  end

  def find(word)
    if @entries.keys.include?(word)
      p "1"
      return @entries.select {|k, v| k == word}
    end
    @entries.keys.each do |w|
      if w.include?(word)
        return @entries.select {|k, v| k.include?(word)}
      end
    end
    if @entries.keys.include?(word) == false
      return {}
    end
  end

  def printable
    sorted =  @entries.to_a.sort
    ans = []
    sorted.each do |arr|
      ans << %Q([#{arr[0]}] "#{arr[1]}")
    end
    ans.join("\n")
  end



end
