class Friend
  def greeting(name = nil)
    if name == nil
      p "Hello!"
    else
      p "Hello, #{name}!"
    end
  end
end
