class Timer

attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    ans = []

    if @seconds%60 < 10
      ans << "0#{@seconds%60}"
    else
      ans << @seconds%60
    end

    if (@seconds%3600)/60 < 10
      ans << "0#{(@seconds%3600)/60}"
    elsif @seconds/60 > 10
      ans << "#{(@seconds%3600)/60}"
    end

    if @seconds/3600 < 10
      ans << "0#{@seconds/3600}"
    elsif @seconds/3660 > 10
      ans << "#{@seconds/3600}"
    end

    ans.reverse.join(":")
  end
end
