class Book

attr_accessor :title

  def title=(title)
    excludes = ["the", "a", "an", "of", "in", "and"]
    ans = []
    title.split(" ").each do |w|
      if excludes.include?(w)
        ans << w
      else
        ans << w.capitalize
      end
    end
    ans[0].capitalize!
    @title = ans.join(" ")
  end
end
